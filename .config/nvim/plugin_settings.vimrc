" Set in here instead of general.vimrc because it requires a plugin
" This does 'set background=dark' for us
colorscheme monokai
let g:monokai_term_italic=1
" Flip the left/right resize keys so it isn't (is?) inverted
" 104 is 'l' and 108 is 'h'
let g:winresizer_keycode_left = 104
let g:winresizer_keycode_right = 108

" Remap <ctrl-n> to toggle nerdtree, since it's the same as <j> anyways
nnoremap <silent> <C-n> :NERDTreeToggle<CR>

" A function to find either project-local config files, or to fallback to my
" saved config file.
function! s:GetFormatConf(conf_name, default_name)
	" globpath finds the config file (or nothing), fnamemodify turns it into an absolute path
	let l:confpath = fnamemodify(globpath('.', a:conf_name), ':p')

	" Actually check if there's the desired config type in the current dir
	if filereadable(l:confpath)
		return l:confpath
	else
		" My fall-back config file
		return '~/dev/dotfiles/formatter-configs/' . a:default_name
	endif
endfunction

" This defines uncrustify with the current local cfg, or my fall-back
" Although it won't dynamically reload, so close vim if changing projects...
let g:formatdef_uncrustify = "'" . "uncrustify -q -l CPP -c " . s:GetFormatConf('*.cfg', 'uncrustify.cfg') . "'"
let g:formatters_java = ['uncrustify']
" A boolean of if we're using tabs or not
let g:formatter_use_tabs = (&expandtab ==? 'noexpandtab') ? "true" : "false"
" LuaFormatter
let g:formatdef_luaformat = "'" . "lua-format -c " . s:GetFormatConf('lua-format', 'lua-format') . "'"
let g:formatdef_prettier_markdown = '"prettier --parser markdown --use-tabs " . g:formatter_use_tabs . " --tab-width ".&shiftwidth'
let g:formatdef_prettier_json = '"prettier --parser json --use-tabs " . g:formatter_use_tabs . " --tab-width ".&shiftwidth'
let g:formatdef_prettier_javascript = '"prettier --use-tabs " . g:formatter_use_tabs . " --tab-width ".&shiftwidth'
let g:formatdef_prettier_html = '"prettier --parser html --use-tabs " . g:formatter_use_tabs . " --tab-width ".&shiftwidth'

" Check if we're using tabs
if &expandtab ==? 'noexpandtab'
	" A 0 passed to -i signifies tabs
	let g:formatdef_shfmt = '"shfmt -s -i 0"'
else
	" shfmt uses non-zero values to the -i flag for spaces
	let g:formatdef_shfmt = '"shfmt -s -i ".&shiftwidth'
endif

" Prefer astyle only if the working dir has a config file
if filereadable('.astylerc')
	let g:formatters_cpp = ['astyle_cpp']
	let g:formatters_c = ['astyle_cpp']
else
	" Otherwise just use uncrustify
	let g:formatters_cpp = ['uncrustify']
	let g:formatters_c = ['uncrustify']
endif
"FIXME
let g:formatters_lua = ['luaformat']
let g:formatters_markdown = ['prettier_markdown']
let g:formatters_sh = ['shfmt']
let g:formatters_PKGBUILD = ['shfmt']
let g:formatters_javascript = ['prettier_javascript']
let g:formatters_json = ['prettier_json']
let g:formatters_html = ['prettier_html']
" Disables the fallback formatting if vim-autoformat doesn't find a formatter
let g:autoformat_autoindent = 0
let g:autoformat_retab = 0
let g:autoformat_remove_trailing_spaces = 0

" Makes vim-autoformat very verbose
" Useful for debugging it
"let g:autoformat_verbosemode=1

" A custom var to toggle autoformat on/off as needed
let s:autoformat_run_on_save = 1
" For when you know you want it disabled
function! s:DisableAutoformat()
	let s:autoformat_run_on_save = 0
	echom 'Disabled auto-formatting on-save'
endfunction
" For when you know you want it enabled
function! s:EnableAutoformat()
	let s:autoformat_run_on_save = 1
	echom 'Enabled auto-formatting on-save'
endfunction

command! DisableAutoformat call s:DisableAutoformat()
command! EnableAutoformat call s:EnableAutoformat()

" Run autoformat only if enabled
function! g:RunAutoformatIfEnabled()
	if (s:autoformat_run_on_save)
		" Runs the command created by vim-autoformat plugin
		Autoformat
	endif
endfunction

" Tells vim-autoformat to run on-save
au BufWrite * :call RunAutoformatIfEnabled()

" Sets the Airline plugin theme on startup (requires airline_themes plugin)
" This is my old preferred theme
let g:airline_theme = 'minimalist'
" Tells Airline to reskin the tabline as well
let g:airline#extensions#tabline#enabled = 1
" enable/disable coc integration
let g:airline#extensions#coc#enabled = 1
" Add line number to the error/warning messages
let airline#extensions#coc#stl_format_err = '%E{[%e(#%fe)]}'
let airline#extensions#coc#stl_format_warn = '%W{[%w(#%fw)]}'

" The PDF viewer used to displays the live version of your LaTeX
let g:livepreview_previewer = 'zathura'

" ----------------------------------------
" Begin Coc.nvim garbage below!
" ----------------------------------------

" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1):
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if has('patch8.1.1068')
  " Use `complete_info` if your (Neo)Vim version supports it.
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use `[g` and `]g` to navigate diagnostics
" nmap <silent> [g <Plug>(coc-diagnostic-prev)
" nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
" nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
" xmap <leader>a  <Plug>(coc-codeaction-selected)
" nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current line.
" nmap <leader>ac  <Plug>(coc-codeaction)
" " Apply AutoFix to problem on the current line.
" nmap <leader>qf  <Plug>(coc-fix-current)

" Introduce function text object
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
" xmap if <Plug>(coc-funcobj-i)
" xmap af <Plug>(coc-funcobj-a)
" omap if <Plug>(coc-funcobj-i)
" omap af <Plug>(coc-funcobj-a)

" Use <TAB> for selections ranges.
" NOTE: Requires 'textDocument/selectionRange' support from the language server.
" coc-tsserver, coc-python are the examples of servers that support it.
" nmap <silent> <TAB> <Plug>(coc-range-select)
" xmap <silent> <TAB> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings using CoCList:
" Show all diagnostics.
" nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" " Manage extensions.
" nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" " Show commands.
" nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" " Find symbol of current document.
" nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" " Search workspace symbols.
" nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" " Do default action for next item.
" nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" " Do default action for previous item.
" nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" " Resume latest coc list.
" nnoremap <silent> <space>p  :<C-u>CocListResume<CR>
