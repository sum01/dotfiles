" Uses Plug to initialize our plugins
if has("win32")
	" Strictly keybinding changes
	source $HOME/AppData/Local/nvim/keybindings.vimrc
	" Anything not belonging to the other categories
	source $HOME/AppData/Local/nvim/general.vimrc
	source $HOME/AppData/Local/nvim/init_plugins.vimrc
	" Sets plugin-specific settings (must be after init_plugins)
	source $HOME/AppData/Local/nvim/plugin_settings.vimrc
else
	" Strictly keybinding changes
	source $HOME/.config/nvim/keybindings.vimrc
	" Anything not belonging to the other categories
	source $HOME/.config/nvim/general.vimrc
	source $HOME/.config/nvim/init_plugins.vimrc
	" Sets plugin-specific settings (must be after init_plugins)
	source $HOME/.config/nvim/plugin_settings.vimrc
endif
