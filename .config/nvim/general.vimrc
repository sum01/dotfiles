" Note that the colorscheme is set in plugin_settings because it requires the Monokai plugin
" Use true-color for colorscheme - requires a terminal emulator that supports true-color
set termguicolors
" Disable the max line length visual bar
set colorcolumn=0
" Enables the line number ruler
set number
" Sets tab width to 2
set tabstop=2
" Don't use softtabs
set softtabstop=0
" Actually use tabs over spaces
set noexpandtab
" ??? Some more tab display stuff
set shiftwidth=2
" Enable mouse support
set mouse=a
" Tells splits to go below the current thing
set splitbelow
" Tells splits to go to the right instead of left
set splitright
" Disables the preview window from opening when auto-completers (such as YouCompleteMe) trigger it.
set completeopt-=preview
" Ignores case when searching
set ignorecase
" If ignorecase is on with this, then lower-case searches ignore case....
" but put any upper-case in the search and it's suddenly not ignoring case.
set smartcase

" Enables spellchecking
"set spell

" Shows what's changed in a specific buffer since last save
" command DiffOrig vert new | set buftype=nofile | read ++edit # | 0d_
" 		\ | diffthis | wincmd p | diffthis
