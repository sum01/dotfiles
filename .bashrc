# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Disables closing the terminal with Ctrl-D keybinding
set -o ignoreeof

# Pull in my env vars (browser, editor, etc.)
source ~/.profile

# Various ENVVARS for all sorts of stuff
export PS1='[\u@\h \W]\$ '
export GOPATH="$HOME/dev/gopath"
export WINEARCH='win64'

alias ls='ls --color=auto'
# Editor shortcuts
alias vi=$EDITOR
alias vim=$EDITOR
alias n=$EDITOR
alias gcc='gcc -Werror -Wpedantic -Wall -fsanitize=address,undefined,null'
alias gcc_raw='gcc'
alias 'g++'='g++ -Werror -Wpedantic -Wall -fsanitize=address,undefined,null'
alias 'g++_raw'='g++'
# Use colortheme appropriate for the dark terminal background
alias delta='delta --dark'
# Use color & show hidden files with tree command
alias tree="tree -C -I '.git'"
# Use my preferred colortheme
alias btm='btm --color nord'
# "Super cd" - it drops back to home, then uses fzf & dirname to cd anywhere very quickly
alias fcd='cd "$HOME" && _fzfdir=$(fzf) && if [[ -d "$_fzfdir" ]]; then cd "$_fzfdir"; else cd $(dirname "$_fzfdir"); fi'
# Only show issues, enable all check types, and suppress that stupid bug message about no includes path
alias cppcheck='cppcheck -q --enable=all --suppress=missingIncludeSystem'
# Make sure exiftool wipes all info on the target file
alias exiftool='exiftool -overwrite_original -all='

# ~~ Shortcuts to bootstrap various project types ~~
# Drops my preferred .editorconfig into the current dir
alias geteconf='cp ~/dev/dotfiles/.editorconfig ./ && echo "Put editorconfig in $(pwd)"'
alias getagpl3='cp /usr/share/licenses/spdx/AGPL-3.0-or-later.txt ./LICENSE && echo "Put AGPL3 in $(pwd)"'
# Gets my preferred uncrustify config
alias getuncrust='cp ~/dev/dotfiles/formatter-configs/uncrustify.cfg ./ && echo "Put Uncrustify config in $(pwd)"'
alias geteslint='cp ~/dev/dotfiles/formatter-configs/.eslint.json ./ && echo "Put Eslint config in $(pwd)"'
alias getluaconf='cp ~/dev/dotfiles/formatter-configs/lua-format ./ && echo "Put lua-format config in $(pwd)"'
# A command to start a new git project which gets stuff that's (almost) always needed
alias gitinit='git init && geteconf && getagpl3 && cp ~/dev/dotfiles/.gitattributes ./'
# A quickstart for new C++ projects | Seperate uncrustify getter since I don't want to hold it in the project-templates/
alias newcpp='cp -r ~/dev/dotfiles/project-templates/cpp/. ./ && getuncrust && gitinit'
alias newjavascript='cp -r ~/dev/dotfiles/project-templates/js/. ./ && geteslint && gitinit'
alias newaur='cp -r ~/dev/dotfiles/project-templates/aur/. ./ && geteconf'

# Refreshes the mirrorlist automatically to use USA-based servers only using https
alias updatemirrors='sudo reflector --latest 12 --country US --age 12 --protocol https --sort rate --save /etc/pacman.d/mirrorlist'

# Use a black screen on the lock screen
alias i3lock='i3lock -c 000000'
# Always use the conf file for vimiv
alias vimiv="vimiv --config=$HOME/.config/vimiv/vimivrc"

# This compiles a cmake program and runs tests in 1 command.
# It also creates the compile_commands.json needed by CCLS.
function cmakebuild() {
	local _build_fresh=0
	local _build_type='Debug'
	local _build_shared=1
	# c++ is a symlink to the system's default compiler
	local _cpp_compiler='c++'
	local _run_tests=0
	local _export_compile_cmds='ON'
	local _extra_cmake_args=''

	# Weird syntax to loop through args instead of using getopts
	# See https://unix.stackexchange.com/a/55754
	while [[ $# -gt 0 ]]; do
		case $1 in
		--)
			shift
			local _extra_cmake_args=("$@")
			break
			;;
		-f | --fresh)
			local _build_fresh=1
			;;
		-rwdi | --relwithdebinfo)
			local _build_type='RelWithDebInfo'
			local _export_compile_cmds='OFF'
			;;
		-msr | --minsizerel)
			local _build_type='MinSizeRel'
			local _export_compile_cmds='OFF'
			;;
		-r | --release)
			local _build_type='Release'
			local _export_compile_cmds='OFF'
			;;
		-s | --static)
			local _build_shared=0
			;;
		-c | --clang)
			# local _c_compiler='clang'
			local _cpp_compiler='clang++'
			;;
		-t | --test)
			local _run_tests=1
			;;
		-h | --help)
			echo "
This creates a build/{static,shared}/{debug,release} dir
and easily compiles a Cmake program for you.

Defaults are to use GCC, compile debug, and use shared libraries.
This script is NOT meant to be used with a multi-config compiler (like MSVC)!

	~~~ Optional cmakebuild args ~~~

	-f (or --fresh) to do a fresh build (deletes the relevant dir before building!).
	-s (or --static) to build static.
	-c (or --clang) to build with Clang.
	-t (or --test) to run tests.
	-r (or --release) to build release.
	-rwdi (or --relwithdebinfo) to build release with debugger info.
	-msr (or --minsizerel) to build a minimum size release.

To pass args directly to the Cmake command, use a double-dash after cmakebuild, then the args.
Like so: cmakebuild -- -DSOME_CMAKE_ARG=VALUE

So as an example, say we want a fresh, static build, and to set HTTPLI_SPLIT to ON...
cmakebuild -f -s -- -DHTTPLIB_SPLIT=ON

Note: do NOT pass CMAKE_BUILD_TYPE as an additional arg in this manner!"
			return 0
			;;
		*)
			echo 'Unrecognized arg passed to cmakebuild. For help, use cmakebuild -h'
			return 0
			;;
		esac
		shift
	done

	# All of this is just an easy way for us to define the build dir
	# since the options are build/{static,shared}/{debug,release}
	# ${_build_type,,} makes the string lower-case.
	local _build_dir="build/shared/${_build_type,,}"
	if [[ $_build_shared == 0 ]]; then
		local _build_dir="build/static/${_build_type,,}"
	fi

	# Do this after the switch-case in case they changed build dir with release/debug modes
	if [[ $_build_fresh == 1 ]]; then
		rm -rf $_build_dir
	fi

	# Make the build dir if it doesn't exist.
	mkdir -p $_build_dir

	# CMAKE_EXPORT_COMPILE_COMMANDS is for CCLS (language server).
	# -B tells it to put build files in that build dir.
	# ${_export_compile_cmds[@]} is telling bash to print the whole array.
	cmake -DCMAKE_BUILD_TYPE="$_build_type" \
		-DCMAKE_EXPORT_COMPILE_COMMANDS="$_export_compile_cmds" \
		-DBUILD_SHARED_LIBS="$_build_shared" \
		-DCMAKE_CXX_COMPILER="$_cpp_compiler" \
		-B$_build_dir . \
		"${_extra_cmake_args[@]}"

	# Wrap this in a if since some projects don't have compile_commands.
	# such as header-only (since they don't actually compile).
	# otherwise, you get an error
	if [[ -f "$_build_dir/compile_commands.json" ]]; then
		# Put the compile_commands in a place where vim will see it
		# These are used by ccls
		# Not using symlinks in case the build dir is deleted
		cp -f "$_build_dir/compile_commands.json" ./
	fi
	# Build the main targets with all cores
	cmake --build $_build_dir -- -j$(nproc)
	# If there's a Testing dir, there are CTest tests available to build
	if [[ $_run_tests == 1 ]]; then
		if [[ -d "$_build_dir/Testing" ]]; then
			# This runs the tests, and the additional ARGS tell it to print errors from the tests
			cmake --build $_build_dir --target test -- -j$(nproc) ARGS="-C Debug --output-on-failure"
		else
			echo "Couldn't find any tests to run!"
		fi
	fi
}

# For making sure I log out after pushing a Docker image to Gitlab
function dockerpush() {
	if [[ $1 == "" ]]; then
		echo 'Did not pass a docker tag name to push'
		return 1
	fi
	docker login registry.gitlab.com
	docker push $1
	# We log out since passwords get stored plaintext
	docker logout registry.gitlab.com || echo 'ERROR: Docker did not log out of Gitlab!'
}

# Keychain remembers ssh passwords (after 1 use) until reboot
# Makes multiple git pushes much less painful
# confhost tells it to load the ~/.ssh/config
eval $(keychain --eval --quiet --confhost)

function update_po() {
	# Update the list of files using gettext
	# Sort because rg seems to output in random order each call
	rg -l 'gettext' src/ | sort >po/POTFILES.in
	# Check if we're not inside the po dir
	if [[ $(basename $(pwd)) != "po" ]]; then
		# Try to cd into po
		if ! [[ -d "$(pwd)/po" ]]; then
			echo "Failed to find a po dir, stopping early"
			return 1
		else
			cd po
		fi
	fi
	echo 'Updating pot & po files...'
	intltool-update -p -g $pwd &&
		intltool-update -s -g $pwd &&
		for _pofile in $(find . -regextype posix-extended -regex '.+\.po$'); do intltool-update $(basename -s .po $_pofile) -g $pwd; done
	# cd back to the original dir
	cd ..
	echo 'Finished updating pot & po files'
}

function common_programs() {
	echo "
	PC cpu/mem usage: btm
	cat alternative: bat
	find alternative: fd
	grep alternative: rg
	ps alternative: procs
	Count lines of code: tokei

	Screen config: arandr
	Terminal audio settings: alsamixer
	GUI audio settings: pavucontrol
	GIF program: peek
	Matlab alternative: octave
	GPU monitor: radeontop
	Music tagger: picard
	UML: dia
	Matrix: fractal
	Changing defaults: xdg-settings
	PDF conversion: calibre
	"
}
